from flask import Flask
from flask_serial import Serial
from flask_socketio import SocketIO, emit
from flask_cors import CORS, cross_origin
import json
import eventlet
import re
import datetime

eventlet.monkey_patch()
app = Flask(__name__)
app.config['SERIAL_TIMEOUT'] = 1
app.config['SERIAL_PORT'] = 'COM17'
app.config['SERIAL_BAUDRATE'] = 38400
app.config['SERIAL_BYTESIZE'] = 8
app.config['SERIAL_PARITY'] = 'N'
app.config['SERIAL_STOPBITS'] = 1
app.config['CORS_HEADERS'] = 'Content-Type'

ser = Serial(app)
socketio = SocketIO(app)
cors = CORS(app)

currentDT = datetime.datetime.now()
newlist = []

# instrument info dictionary
instrumentInfo = {"Instrument": "ISE-4000", "Date": datetime.datetime.today().strftime("%Y-%m-%d"), "Time": currentDT.strftime("%H:%M:%S")}

escapeChar = re.compile(r'[^a-zA-VX-YZ/]')
escapeDouble = re.compile(r'[^0-9.,]')

@app.route('/ise4000')
@cross_origin()
def index():
    return "Hello, I am c-2000"

@ser.on_message()
def handle_message(msg):
    # incoming serial message = b'\x1b@\x1b@\r\x1bW\x02  PT( 1)\x1bW\x01\r sec |Ratio| INR | Mean\r 6.56,0.547,0.547,\r -----Normal Range-----\r10.70 < sec < 14.30\r\r'
    # removing byte control char
    c2000Dic = espMsg(msg.decode('utf-8'))
    c2000Result = Merge(instrumentInfo, c2000Dic)

    socketio.emit("c2000_message", data={"message": json.dumps(c2000Result)}, brodcast=True)

@ser.on_log()
def handle_logging(level, info):
    print(level, info)

@socketio.on('connect')
def test_connect():
    emit('after connect', {'data': 'Hello I am c-2000'})

def espMsg(rawMsg):
    msgStr = escapeChar.sub(' ', rawMsg)
    msgDub = escapeDouble.sub(' ', rawMsg)

    rawMsgSplit = rawMsg.strip.split('\r')

    listMsg = msgStr.split()
    listDub = msgDub.split()

    if (listMsg[0] == 'PT'):
        nameData = ['lot No', 'sec,Ratio,INR,Mean', 'Normal Range(min)', 'Normal Range(max)', 'unit', 'Test Name']
        listDub.append(listMsg[len(listMsg) - 1])
        listDub.append(listMsg[0])
        dicRes = dict(zip(nameData, listDub))
    elif (listMsg[0] == 'APTT'):
        nameData = ['lot No', 'sec,Mean', 'Normal Range(min)', 'Normal Range(max)', 'unit', 'Test Name']
        listDub.append(listMsg[len(listMsg) - 1])
        listDub.append(listMsg[0])
        dicRes = dict(zip(nameData, listDub))
    elif (listMsg[0] == 'FIB'):
        nameData = ['lot No', 'sec,g/l,Mean', 'Normal Range(min)', 'Normal Range(max)', 'unit', 'Test Name']
        listDub.append(listMsg[len(listMsg) - 1])
        listDub.append(listMsg[0])
        dicRes = dict(zip(nameData, listDub))
    elif (listMsg[0] == 'TT'):
        nameData = ['lot No', 'sec,Mean', 'Normal Range(min)', 'Normal Range(max)', 'unit', 'Test Name']
        listDub.append(listMsg[len(listMsg) - 1])
        listDub.append(listMsg[0])
        dicRes = dict(zip(nameData, listDub))
    elif (listMsg[0] == 'II'):
        nameData = ['lot No', 'sec,%,Ul/mL,Mean', 'Normal Range(min)', 'Normal Range(max)', 'unit', 'Test Name']
        newlist.append(listDub[0])
        newlist.append(rawMsgSplit[3])
        newlist.append(listDub[4])
        newlist.append(listDub[5])
        newlist.append(listMsg[len(listMsg) - 1])
        newlist.append(listMsg[0])
        dicRes = dict(zip(nameData, newlist))
    elif (listMsg[0] == 'V'):
        nameData = ['lot No', 'sec,%,Ul/mL,Mean', 'Normal Range(min)', 'Normal Range(max)', 'unit', 'Test Name']
        newlist.append(listDub[0])
        newlist.append(rawMsgSplit[3])
        newlist.append(listDub[4])
        newlist.append(listDub[5])
        newlist.append(listMsg[len(listMsg) - 1])
        newlist.append(listMsg[0])
        dicRes = dict(zip(nameData, newlist))
    elif (listMsg[0] == 'VII'):
        nameData = ['lot No', 'sec,%,Ul/mL,Mean', 'Normal Range(min)', 'Normal Range(max)', 'unit', 'Test Name']
        newlist.append(listDub[0])
        newlist.append(rawMsgSplit[3])
        newlist.append(listDub[4])
        newlist.append(listDub[5])
        newlist.append(listMsg[len(listMsg) - 1])
        newlist.append(listMsg[0])
        dicRes = dict(zip(nameData, newlist))
    elif (listMsg[0] == 'X'):
        nameData = ['lot No', 'sec,%,Ul/mL,Mean', 'Normal Range(min)', 'Normal Range(max)', 'unit', 'Test Name']
        newlist.append(listDub[0])
        newlist.append(rawMsgSplit[3])
        newlist.append(listDub[4])
        newlist.append(listDub[5])
        newlist.append(listMsg[len(listMsg) - 1])
        newlist.append(listMsg[0])
        dicRes = dict(zip(nameData, newlist))
    elif (listMsg[0] == 'VIII'):
        nameData = ['lot No', 'sec,%,Ul/mL,Mean', 'Normal Range(min)', 'Normal Range(max)', 'unit', 'Test Name']
        newlist.append(listDub[0])
        newlist.append(rawMsgSplit[3])
        newlist.append(listDub[4])
        newlist.append(listDub[5])
        newlist.append(listMsg[len(listMsg) - 1])
        newlist.append(listMsg[0])
        dicRes = dict(zip(nameData, newlist))
    elif (listMsg[0] == 'IX'):
        nameData = ['lot No', 'sec,%,Ul/mL,Mean', 'Normal Range(min)', 'Normal Range(max)', 'unit', 'Test Name']
        newlist.append(listDub[0])
        newlist.append(rawMsgSplit[3])
        newlist.append(listDub[4])
        newlist.append(listDub[5])
        newlist.append(listMsg[len(listMsg) - 1])
        newlist.append(listMsg[0])
        dicRes = dict(zip(nameData, newlist))
    elif (listMsg[0] == 'XI'):
        nameData = ['lot No', 'sec,%,Ul/mL,Mean', 'Normal Range(min)', 'Normal Range(max)', 'unit', 'Test Name']
        newlist.append(listDub[0])
        newlist.append(rawMsgSplit[3])
        newlist.append(listDub[4])
        newlist.append(listDub[5])
        newlist.append(listMsg[len(listMsg) - 1])
        newlist.append(listMsg[0])
        dicRes = dict(zip(nameData, newlist))
    elif (listMsg[0] == 'XII'):
        nameData = ['lot No', 'sec,%,Ul/mL,Mean', 'Normal Range(min)', 'Normal Range(max)', 'unit', 'Test Name']
        newlist.append(listDub[0])
        newlist.append(rawMsgSplit[3])
        newlist.append(listDub[4])
        newlist.append(listDub[5])
        newlist.append(listMsg[len(listMsg) - 1])
        newlist.append(listMsg[0])
        dicRes = dict(zip(nameData, newlist))
    return dicRes

def Merge(dict1, dict2):
    return {**dict1, **dict2}

if __name__ == '__main__':
    socketio.run(app, host='192.168.88.121', port=5001, debug=False)
